
AutoVPN
=======

A simple self-service OpenVPN infrastructure (with X.509 PKI).


Installation
------------

The 'autovpn' Python package is bundled with 'autoca'.
Once you've got the autoca/autovpn sources, just run::

    $ sudo python setup.py install

from the top-level directory.


Configuration
-------------

How to run the AutoVPN web application will depend on the deployment
method that you choose (mod_wsgi, FastCGI, standalone HTTP server...).
Examples of a few different drivers can be found in the apps/
directory.

In all cases though, you'll need to create a configuration file and
point the application at it (for example using the VPN_APP_CONFIG
environment variable). This file should define the following variables
(using Python syntax):

VPN_ENDPOINT
  DNS name for the VPN server (use more than one A record for
  crude load balancing)

VPN_SITE_URL
  The public URL for the AutoVPN web application

VPN_CERT_VALIDITY
  How many days should the generated certificates be valid for
  (default: 7)

VPN_DEFAULT_SUBJECT_ATTRS
  A dictionary containing X.509 attributes to use as defaults for
  the subject of the generated certificates. For example::

    VPN_DEFAULT_SUBJECT_ATTRS = {'O': 'My VPN', 'C': 'IT'}

  The app will only set the 'CN' attribute.

There are two different options to connect to the CA, you can either
connect to a remote autoca web application:

VPN_CA_URL
  URL of the remote autoca web application

CA_SHARED_SECRET
  (optional) the shared secret to use for authentication

Otherwise, you can instantiate a CA that is local to the AutoVPN
application itself (for simpler deployments):

VPN_CA_ROOT
  Base directory for the CA storage -- keep it private!

VPN_CA_BITS
  How many bits to use for the CA RSA key (default 4096)

VPN_CA_SUBJECT
  Dictionary with the X.509 CA subject attributes

In this latter case, the autoca web application will be available
under the /ca/ URL prefix.

The VPN web application supports authentication (to control who has
access to the certificate generation). To enable it, define the
following variables:

AUTH_ENABLE
  Set this to True to enable authentication

AUTH_FUNCTION
  Set this to a function that should accept two arguments (username
  and password) and return a True/Fals result.

